package com.file.encoding.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Component
@JsonIgnoreProperties()
public class FileInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getEncodingType() {
		return encodingType;
	}
	public void setEncodingType(String encodingType) {
		this.encodingType = encodingType;
	}
	public String getFileActionType() {
		return fileActionType;
	}
	public void setFileActionType(String type) {
		this.fileActionType = type;
	}
	public String getFileHashKeyValue() {
		return fileHashKeyValue;
	}
	public void setFileHashKeyValue(String fileHashKeyValue) {
		this.fileHashKeyValue = fileHashKeyValue;
	}
	String filename;
	String encodingType;
	String fileActionType;
	String folderPath;
	String fileHashKeyValue;
}
