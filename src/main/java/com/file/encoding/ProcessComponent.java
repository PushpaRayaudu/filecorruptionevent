package com.file.encoding;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.file.encoding.dto.FileInfo;

@Component
public class ProcessComponent {

	@KafkaListener(topics = "foldertopic", groupId = "group-id")
	public void listenGroupFoo(FileInfo event) {
		System.out.println("Received Message " + event.getFilename());
	}

}
