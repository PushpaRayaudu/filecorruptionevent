package com.file.encoding.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.file.encoding.dto.FileCorruptionInfo;
import com.file.encoding.model.Alertoncorruption;

@Repository
@Transactional
public class AlertOnFileChangeRepo {

	@Autowired
	AlertOnCourruptionModelRepository alertOnChangeRepo;

	public List<Alertoncorruption> getAlerts() {
		return alertOnChangeRepo.findAll();

	}

	public void saveAlert(FileCorruptionInfo fileinfo) {
		Alertoncorruption alertInfo = new Alertoncorruption();
		alertInfo.setCorruptionEventType(fileinfo.getCorruptionEventType());
		alertInfo.setFileName(fileinfo.getFileName());
		alertInfo.setFolderName(fileinfo.getFileFolder());
		alertInfo.setStatusCode(409);
		alertInfo.setComments(fileinfo.getHttpStatusCode());
		alertInfo.setLastmodified(new java.sql.Date(new java.util.Date().getTime()));
		alertOnChangeRepo.save(alertInfo);

	}

}
