package com.file.encoding.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Alertoncorruption {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String fileName;
	private String folderName;
	private String corruptionEventType;
	private int statusCode;
	private String comments;
	private Date lastmodified;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the lastmodified
	 */
	public Date getLastmodified() {
		return lastmodified;
	}

	/**
	 * @param lastmodified the lastmodified to set
	 */
	public void setLastmodified(Date lastmodified) {
		this.lastmodified = lastmodified;
	}

	/**
	 * @return the corruptionEventType
	 */
	public String getCorruptionEventType() {
		return corruptionEventType;
	}

	/**
	 * @param corruptionEventType the corruptionEventType to set
	 */
	public void setCorruptionEventType(String corruptionEventType) {
		this.corruptionEventType = corruptionEventType;
	}

	@Override
	public String toString() {
		return "AlertOnFileEncodeChange [id=" + id + ", fileName=" + fileName + ", folderName=" + folderName
				+ ", CorruptionType=" + corruptionEventType + "+ , statusCode=" + statusCode + "+, comments=" + comments
				+ "]";
	}

}
