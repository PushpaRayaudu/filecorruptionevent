package com.file.encoding.dto;

public class CorruptionType {
	
	private String corruptionType;
	private String httpStatusCode;
	/**
	 * @return the corruptionType
	 */
	public String getCorruptionType() {
		return corruptionType;
	}
	/**
	 * @param corruptionType the corruptionType to set
	 */
	public void setCorruptionType(String corruptionType) {
		this.corruptionType = corruptionType;
	}
	/**
	 * @return the httpStatusCode
	 */
	public String getHttpStatusCode() {
		return httpStatusCode;
	}
	/**
	 * @param httpStatusCode the httpStatusCode to set
	 */
	public void setHttpStatusCode(String httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	

}
