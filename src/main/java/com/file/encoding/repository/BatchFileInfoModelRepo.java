package com.file.encoding.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.file.encoding.model.BatchFileInfo;

@Repository
public interface BatchFileInfoModelRepo extends JpaRepository<BatchFileInfo,Integer>{

	List<BatchFileInfo> findByFileName(String fileName);

	
}
