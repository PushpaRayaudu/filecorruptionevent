package com.file.encoding.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.file.encoding.model.Alertoncorruption;

@Repository
public interface AlertOnCourruptionModelRepository extends JpaRepository<Alertoncorruption, Integer>{
	
}
