package com.file.encoding.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BatchFileInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String fileName;
	private String folderName;
	/**
	 * @return the hashkey
	 */
	public String getHashkey() {
		return hashkey;
	}

	/**
	 * @param hashkey the hashkey to set
	 */
	public void setHashkey(String hashkey) {
		this.hashkey = hashkey;
	}

	private String encodingType;
	private String hashkey;
	private Date  lastmodified ;

	/**
	 * @return the lastmodified
	 */
	public Date getLastmodified() {
		return lastmodified;
	}

	/**
	 * @param lastmodified the lastmodified to set
	 */
	public void setLastmodified(Date lastmodified) {
		this.lastmodified = lastmodified;
	}

	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getEncodingType() {
		return encodingType;
	}

	public void setEncodingType(String encodingType) {
		this.encodingType = encodingType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "FileEncodingInfo [id=" + id + ", fileName=" + fileName + ", folderName=" + folderName
				+ ", encodingType=" + encodingType + "]";
	}

}
