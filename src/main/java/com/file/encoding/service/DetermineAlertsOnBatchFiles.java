package com.file.encoding.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.file.encoding.dto.FileCorruptionInfo;
import com.file.encoding.dto.FileInfo;
import com.file.encoding.model.BatchFileInfo;
import com.file.encoding.repository.ComparingDbEntryForCourruption;

@Service
public class DetermineAlertsOnBatchFiles {

	@Autowired
	ComparingDbEntryForCourruption reposi;

	@Autowired
	private KafkaTemplate<String, FileCorruptionInfo> kafkaTemplate;

	@Value(value = "${spring.kafka.topic}")
	private String kafkaTopic;

	public FileCorruptionInfo findCorruptionOnFiles(FileInfo fileInfo) {
		FileCorruptionInfo corruptionDto = new FileCorruptionInfo();
		corruptionDto.setFileFolder(fileInfo.getFolderPath());
		corruptionDto.setFileName(fileInfo.getFilename());
		detrmineAnyCourruptionsOnFile(fileInfo, corruptionDto);
		return corruptionDto;

	}

	public FileCorruptionInfo detrmineAnyCourruptionsOnFile(FileInfo fileInfo, FileCorruptionInfo corruptionDto) {

		encodeHashKeyCompareWithDB(fileInfo, corruptionDto);
		if (corruptionDto.getHttpStatusCode().contains("409")) {
			kafkaTemplate.send(kafkaTopic, corruptionDto);

		}
		return corruptionDto;
	}

	public FileCorruptionInfo encodeHashKeyCompareWithDB(FileInfo fileInfo, FileCorruptionInfo corruptionDto) {

		List<BatchFileInfo> dbRecorList = reposi.getFileInfo(fileInfo.getFilename());
		BatchFileInfo dbRecord = null;
		if (dbRecorList.isEmpty()) {
			reposi.persistFileInfo(fileInfo);
			System.out.println("File got saved into the DB with the httpStatusCode 201");
			corruptionDto.setHttpStatusCode("201 - Insert file Encoding and HashKey");
		} else {
			dbRecord = dbRecorList.get(0);
			System.out.println(dbRecord.getEncodingType() + ":::" + dbRecord.getHashkey());
			System.out.println("Finding if any corruptions");
			findTheCorruptions(fileInfo, dbRecord, corruptionDto);
		}
		return corruptionDto;
	}

	private FileCorruptionInfo findTheCorruptions(FileInfo fileInfo, BatchFileInfo dbRecord,
			FileCorruptionInfo corruptionDto) {

		if (Objects.nonNull(fileInfo) && Objects.nonNull(dbRecord)) {
			if (!isCorruptionOnEncoding(dbRecord.getEncodingType(), fileInfo.getEncodingType())) {
				corruptionDto.setCorruptionEventType("Encoding");
				corruptionDto.setHttpStatusCode("409 - Conflict with the state of server");
			} else {
				corruptionDto.setHttpStatusCode("200 - File Encoding matched");
			}
			if (isCorruptionOnHashKey(dbRecord.getHashkey(), fileInfo.getFileHashKeyValue())) {
				corruptionDto.getCorruptionEventType().concat("HashKey- Duplicate");
				corruptionDto.setHttpStatusCode("409 - Conflict with the state of server");
			} else {
				reposi.UpdateFileInfoBasedOnHash(fileInfo);
				corruptionDto.setHttpStatusCode("200 - File is not Duplicate");
			}

		}
		return corruptionDto;

	}

	private Boolean isCorruptionOnHashKey(String hashKeyValue, String fileHashKeyValue) {
		return hashKeyValue.equals(fileHashKeyValue);
	}

	private Boolean isCorruptionOnEncoding(String dbencodingType, String encodingTypeFile) {
		return (dbencodingType.equals(encodingTypeFile));

	}

}
