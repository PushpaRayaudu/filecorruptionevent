package com.file.encoding.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.file.encoding.dto.FileInfo;
import com.file.encoding.model.BatchFileInfo;

@Repository
@Transactional
public class ComparingDbEntryForCourruption {

	@Autowired
	BatchFileInfoModelRepo batchFileInfoModelRepo;

	public void persistFileInfo(FileInfo fileinfo) {
		BatchFileInfo fileEncodingInfo = new BatchFileInfo();
		fileEncodingInfo.setEncodingType(fileinfo.getEncodingType());
		fileEncodingInfo.setHashkey(fileinfo.getFileHashKeyValue());
		fileEncodingInfo.setFileName(fileinfo.getFilename());
		fileEncodingInfo.setFolderName(fileinfo.getFolderPath());
		fileEncodingInfo.setLastmodified(new java.sql.Date(new java.util.Date().getTime()));
		batchFileInfoModelRepo.save(fileEncodingInfo);
	}

	public List<BatchFileInfo> getFileInfo(String key) {
		List<BatchFileInfo> fileEncodinglist = batchFileInfoModelRepo.findByFileName(key);
		return fileEncodinglist;

	}

	public void UpdateFileInfoBasedOnHash(FileInfo fileInfo) {
		List<BatchFileInfo> batchFileInfoModelList = batchFileInfoModelRepo.findByFileName(fileInfo.getFilename());
		if (batchFileInfoModelList != null) {
			BatchFileInfo batchFileInfoModelUpdate = batchFileInfoModelList.get(0);
			if (batchFileInfoModelUpdate != null) {
				batchFileInfoModelUpdate.setHashkey(fileInfo.getFileHashKeyValue());
				batchFileInfoModelRepo.save(batchFileInfoModelUpdate);
			}
		}

	}
}