package com.file.encoding;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class FileEventsCheckPoint implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(FileEventsCheckPoint.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Thread.currentThread().join();
		
	}
}