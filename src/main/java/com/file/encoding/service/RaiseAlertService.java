package com.file.encoding.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.file.encoding.dto.FileCorruptionInfo;
import com.file.encoding.repository.AlertOnFileChangeRepo;

@Service
public class RaiseAlertService {

	@Autowired
	AlertOnFileChangeRepo alertsystem;

	public void raiseAlerts(FileCorruptionInfo fileInfo) {

		alertsystem.saveAlert(fileInfo);
		System.err.println("Alert got inserted");

	}

}
